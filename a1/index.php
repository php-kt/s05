<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s5 activity</title>
</head>
<body>

	<?php session_start() ?>
	<?php if(!isset($_SESSION['email'])): ?>
	<div>
		<form method="POST" action="./server.php" style="display: inline-block;">
			<input type="hidden" name="action" value="Login">
			Email: <input type="email" name="email" required /><br/><br/>
			Password: <input type="password" name="pw" required /><br/><br/>
			<button type="submit">Login</button>
		</form>
	</div>
	<?php endif; ?>

	<?php if(isset($_SESSION['email'])): ?>

		<p>Hello, <?php echo $_SESSION["email"]; ?></p>
		<form method="POST" action="./server.php" style="display: inline-block;">
			<br/><br/>
			<input type="hidden" name="action" value="Logout">
			<button type="submit">Logout</button>
		</form>

	<?php endif; ?>


</body>
</html>