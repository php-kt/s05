<?php

session_start();


class TaskList {
	// add
	public function add($description) {
		$newTask = (object) [
			'description' => $description,
			'isFinished' => false
		];

		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}
		array_push($_SESSION['tasks'],$newTask);
	}

	//update
	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	public function remove($id) {
		array_splice($_SESSION['tasks'], $id, 1);
	}

	public function clear() {
		session_destroy();
	}

}

//obj instance

$taskList = new TaskList();

// Add and Update

if ($_POST['action'] === 'add') {
	$taskList->add($_POST['description']);
}

else if ($_POST['action'] === 'update') {
	$taskList->update($_POST['id'],$_POST['description'],$_POST['isFinished']);
}

else if ($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}

else if ($_POST['action'] === 'clear') {
	$taskList->clear();
}

header('Location: ./index.php');

?>