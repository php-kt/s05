<?php

	$tasks = ['Get Git','Bake HTML','Eat CSS','Learn PHP'];

	//check if there is variable that we will declare or pass, null ba or hindi
	if(isset($_GET['index'])) {
		$indexGet = $_GET['index'];
		echo "The retrieved task from GET is $tasks[$indexGet]";
	}
	//nag-add or append yung url natin

	if(isset($_POST['index'])) {
		$indexPOST = $_POST['index'];
		echo "The retrieved task from POST is $tasks[$indexPOST]";
	}

?>



<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s5: Client-Server Communication (GET and POST)</title>
</head>
<body>
	<h1>Task index from GET</h1>

	<form method="GET">

		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>
		<button type="submit">GET</button>
	</form>

	<h1>Task index from POST</h1>
	<form method="POST">

		<select name="index" required>
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
		</select>

		<button type="submit">POST</button>
	</form>
	
</body>
</html>